# -*- coding: utf-8 -*-

from __future__ import (unicode_literals, division, absolute_import, print_function)

__license__ = 'GPL 3'
__copyright__ = '2012, Ruben Pollan <meskio@sindominio.net>'
__docformat__ = 'restructuredtext en'

from calibre.customize import StoreBase

class TheAnarchistLibraryStore(StoreBase):
    name = 'The Anarchist Library'
    description = 'theanarchistlibrary.org is (despite its name) an archive focusing on anarchism, anarchist texts, and texts of interest for anarchists.'
    author = 'Ruben Pollan'
    version = (1, 0, 0)
    drm_free_only = True
    formats = ['EPUB', 'PDF', 'TXT', 'TEX', 'MUSE']
    actual_plugin = 'calibre_plugins.store_theanarchistlibrary.theanarchistlibrary_plugin:TheAnarchistLibraryStore'
